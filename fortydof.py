import os
import requests
import datetime
import sys
import argparse
import pandas as pd
import shutil
from os import mkdir, path
from datetime import datetime

# argument parser
parser = argparse.ArgumentParser(description='Get 40DOF PDF')
parser.add_argument(
    '--all', help='Get all the PDFs from the beginning', action='store_true')
parser.add_argument(
    '--bot', help='Flag to run with Telegram Bot', action='store_true')
args = parser.parse_args(sys.argv[1:])

# variables
today = datetime.today()
todayDay = f"{today:%#d}"
todayMonthNum = f"{today:%m}"
todayMonthWord = f"{today:%B}"
# desktopPath = pathlib.Path.home() / 'Desktop'
thisPath = './'
outputPath = os.path.join(thisPath, f'40 DOF {today:%Y}')


def get_variables(type, date):
    # get variable for url and filename
    filename = f'40.Day-{date:%Y}_{date:%B}-{date:%-d}.pdf'
    if (date == datetime.strptime('2021-07-01', '%Y-%m-%d')):
        url = f'https://lovesingapore.org.sg/40day/{date:%Y}/wp-content/uploads/{date:%Y}/06/'
    elif (date == datetime.strptime('2021-08-01', '%Y-%m-%d')):
        url = f'https://lovesingapore.org.sg/40day/{date:%Y}/wp-content/uploads/{date:%Y}/07/'
    else:
        url = f'https://lovesingapore.org.sg/40day/{date:%Y}/wp-content/uploads/{date:%Y}/{date:%m}/'

    return {
        'filename': filename,
        'url': url,
    }[type]


def requestFile(url, filename):
    # fire the request and save as pdf file
    print(url+filename)
    x = requests.get(url + filename)
    outputFile = os.path.join(outputPath, filename)
    with open(outputFile, 'wb') as f:
        f.write(x.content)
        f.close()
    return url + filename


def main(botBot=False, botDate=today, botAll=False):
    # check that output path exists
    # if not create it
    checkpath = path.exists(outputPath)
    if (checkpath == False):
        mkdir(outputPath)

    # fire the request based on argument of script
    if (args.all or botAll):
        dateString = datetime.strftime(today, '%Y-%m-%d')
        if (os.path.isfile(f'{dateString}.zip')):
            return os.path.abspath(f'{dateString}.zip')

        for date in pd.date_range(start='2021-07-01', end=today):
            url = get_variables('url', date)
            filename = get_variables('filename', date)
            requestFile(url, filename)

        zipFile = shutil.make_archive(dateString, 'zip', outputPath)
        return zipFile
    else:
        filename = get_variables('filename', botDate)
        url = get_variables('url', botDate)
        output = requestFile(url, filename)
        if (args.bot or botBot):
            return output

    # print out all files in folder
    print(os.listdir(outputPath))


if __name__ == '__main__':
    main()
