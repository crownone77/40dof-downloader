#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This program is dedicated to the public domain under the CC0 license.

"""
Simple Bot to reply to Telegram messages.

First, a few handler functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.

Usage:
Basic Echobot example, repeats messages.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

import logging
import os

import fortydof
import sqlite3
import argparse
import sys
import pytz
import dateutil.parser as dateparser
from datetime import datetime, time, timedelta

import telegram
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

# messages
before1july = 'Please enter date starting from 1 July!'
afterToday = 'Date has not arrived'
afterNationalDay = 'It\'s past National Day, nothing to see here'
helpMessage = '/help\nSee this message\n\n/today\nGet Today\'s Passage\n\n/date <DATE>\nGet specific date\'s passage\n\n/all\nGet all pasages from the first one to today'
dontunderstand = 'Sorry I don\'t understand! Press /help for valid commands!'
pleaseenterdate = 'Please enter a date (e.g. 1 July or 2021-07-01)'


class User:
    def __init__(self, id, username, first_name) -> None:
        self.id = id
        self.username = username
        self.first_name = first_name


# Arguments
parser = argparse.ArgumentParser(description='')
parser.add_argument(
    '--send_users', help='Send users today\'s passage', action='store_true')
args = parser.parse_args(sys.argv[1:])
botUser = User('000', 'SYSTEM', 'SYSTEM')


def connect_db():
    # connect to db
    con = sqlite3.connect('ID.db')
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    return (con, cur)


def keep(update, context):
    user = update.effective_user
    (con, cur) = connect_db()

    try:
        cur.execute('insert or ignore into peeps values (?, ?, ?, ?, ?)', (user.id,
                                                                           user.username, user.first_name, user.language_code, user.is_bot))
    except:
        cur.execute(
            'create table peeps(id text not null primary key, username text, first_name text, language_code text, is_bot bool)')
        cur.execute('insert or ignore into peeps values (?, ?, ?, ?, ?)', (user.id,
                    user.username, user.first_name, user.language_code, user.is_bot))

    con.commit()
    con.close()


def log_event(user, event, input=''):
    logger.info(f'[{user.username}, {user.first_name}: {event}] -- {input}')


def start(update, context):
    log_event(update.effective_user, '/start')
    """Send a message when the command /start is issued."""
    message = f'Hi {update.effective_user.first_name}!'
    update.message.reply_text(message)
    keep(update, context)
    help(update, context)


def help(update, context):
    log_event(update.effective_user, '/help')
    """Send a message when the command /help is issued."""
    update.message.reply_text(helpMessage)


def all(update, context):
    log_event(update.effective_user, '/all')

    context.bot.send_chat_action(
        chat_id=update.effective_chat.id, action=telegram.ChatAction.UPLOAD_DOCUMENT)
    zip = fortydof.main(True, today, True)

    zip_file = open(zip, 'rb')
    context.bot.send_document(update.effective_chat.id, zip_file)
    zip_file.close()


def doIt(update, context, inputDate, message='', chat_info=[]):

    filePath = fortydof.main(True, inputDate)

    chat_info = chat_info if chat_info else [(
        update.effective_user.id, update.effective_user.first_name)]

    messageTemplate = message
    for (id, first_name) in chat_info:
        # if (id != '71045113'):
        #     pass
        context.bot.send_chat_action(
            chat_id=id, action=telegram.ChatAction.UPLOAD_DOCUMENT)
        message = messageTemplate.replace('<first_name>', first_name)
        context.bot.send_document(id, filePath, caption=message)


def daily(context: CallbackContext):
    (con, cur) = connect_db()

    chat_info = []
    for row in cur.execute('select * from peeps'):
        row = dict(row)
        chat_info.append((row['id'], row['first_name']))

    doIt('', context, datetime.today(),
         'Good morning <first_name>! Here is your PDF today!', chat_info)
    con.close()


def date(update, context, inputDate=''):
    try:
        args = ' '.join(context.args[0:])
    except:
        args = ''

    log_event(update.effective_user, '/date', args)

    if (not inputDate and not args):
        context.bot.send_message(
            update.effective_user.id, pleaseenterdate, reply_markup=telegram.ForceReply())
        return
    else:
        fuzzyDate = args if args else inputDate

    inputDate = dateparser.parse(fuzzyDate, fuzzy=True)
    logger.info(inputDate)

    if (inputDate < datetime.strptime('2021-07-01', '%Y-%m-%d')):
        update.message.reply_text(before1july)
    elif (inputDate > datetime.today()):
        update.message.reply_text(afterToday)
    elif (inputDate > datetime.strptime('2021-08-09', '%Y-%m-%d')):
        update.message.reply_text(afterNationalDay)
    else:
        doIt(update, context, inputDate)


def today(update, context):
    log_event(update.effective_user, '/today')
    doIt(update, context, datetime.combine(
        datetime.today(), datetime.min.time()))


def echo(update, context):
    user = update.effective_user
    message = update.message

    log_event(user, '', message.text)

    reply = message.reply_to_message if message.reply_to_message else ''
    if (reply and reply.text == pleaseenterdate):
        date(update, context, message.text)
    else:
        if (user.username == 'timot3a'):
            update.message.reply_text(f'you then, {message.text}')
        else:
            update.message.reply_text(dontunderstand)

    # save the message to db for further analysis
    (con, cur) = connect_db()
    try:
        cur.execute('insert or ignore into messages values (?, ?, ?, ?)',
                    (datetime.today(), message.message_id, message.text, user.id))
    except:
        cur.execute("create table messages(datetime_created datetime, message_id text not null primary key, message text, peeps_id text not null, foreign key (peeps_id) references peeps(id))")
        cur.execute('insert or ignore into messages values (?, ?, ?)',
                    (datetime.today(), message.message_id, message.text, user.id))

    con.commit()
    con.close()


def clean_up(context: CallbackContext):
    # download all pasages and put in desktop folder
    # zip up and make available
    log_event(botUser, 'Daily clean up job', 'Job start')
    fortydof.main(botAll=True)

    # delete old zip
    filename = f'{(datetime.today() - timedelta(days=1)).strftime("%Y-%m-%d")}.zip'
    if (os.path.exists(filename)):
        os.remove(filename)

    log_event(botUser, 'Daily clean up job', 'Job completed')


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def main():
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    f = open('secret', 'r')
    secret = f.read()
    f.close()

    updater = Updater(
        secret, use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))
    dp.add_handler(CommandHandler("today", today))
    dp.add_handler(CommandHandler("date", date))
    dp.add_handler(CommandHandler("all", all))

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler(Filters.text, echo))

    # jobs
    j = updater.job_queue
    j.run_daily(daily, days=(0, 1, 2, 3, 4, 5, 6),
                time=time(hour=8, tzinfo=(pytz.timezone('Asia/Singapore'))))
    j.run_daily(clean_up, days=(0, 1, 2, 3, 4, 5, 6),
                time=time(hour=0, minute=30, tzinfo=(pytz.timezone('Asia/Singapore'))))
    # j.run_once(clean_up, 1)

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
