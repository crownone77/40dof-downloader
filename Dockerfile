# syntax=docker/dockerfile:1

FROM python:3.7-slim-buster

WORKDIR /app

RUN \
    mkdir -p /root/Desktop/2021\ 40DOF && \
    pip install requests pandas python-telegram-bot

ENV PYTHONPATH=/usr/lib/python3.7/site-packages

COPY . .

CMD [ "python", "bot.py"]
